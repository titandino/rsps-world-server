import com.rs.game.World
import com.rs.game.content.Toolbelt
import com.rs.game.content.Toolbelt.Tools
import com.rs.game.content.skills.magic.LodestoneAction.Lodestone
import com.rs.utils.Ticks

onLogin { e ->
    e.player.nsv.setL("lastRandom", World.getServerTicks() + Ticks.fromHours(300))
    if (e.player.starter <= 0) {
        for (stone in Lodestone.entries) {
            if (listOf(Lodestone.BANDIT_CAMP, Lodestone.LUNAR_ISLE).contains(stone)) continue
            e.player.unlockLodestone(stone, null)
        }

        for (tool in Tools.entries) {
            if (e.player.toolbelt.get(tool) != null) continue
            e.player.toolbelt.put(tool, 1)
        }
        e.player.addToolbelt(1265)
        Toolbelt.refreshToolbelt(e.player)
    }
}